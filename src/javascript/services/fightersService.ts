import { callApi } from '../helpers/apiHelper';
import type { Fighter } from '../helpers/mockData';

interface FighterServiceInterface {
    getFighters: () => Promise<Fighter[]>;
    getFighterDetails: (id: string) => Promise<Fighter[]>;
}

class FighterService implements FighterServiceInterface {
    async getFighters(): Promise<Fighter[]> {
        try {
            const endpoint: string = 'fighters.json';
            const apiResult = await callApi(endpoint, 'GET');

            return apiResult as Fighter[];
        } catch (error) {
            throw error;
        }
    }

    async getFighterDetails(id: string): Promise<Fighter[]> {
        // todo: implement this method
        // endpoint - `details/fighter/${id}.json`;
        try {
            const endpoint: string = `details/fighter/${id}.json`;
            const apiResult = await callApi(endpoint, 'GET');

            return apiResult as Fighter[];
        } catch (error) {
            throw error;
        }
    }
}

export const fighterService = new FighterService();
