import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';
import { Fighter } from './helpers/mockData';

interface AppInt {
    startApp: () => void;
}

class App implements AppInt {
    constructor() {
        this.startApp();
    }

    static rootElement = document.getElementById('root');
    static loadingElement = document.getElementById('loading-overlay');

    async startApp() {
        try {
            App.loadingElement!.style.visibility = 'visible';

            const fighters: Fighter[] = await fighterService.getFighters();
            const fightersElement: HTMLElement = createFighters(fighters);

            App.rootElement!.appendChild(fightersElement);
        } catch (error) {
            console.warn(error);
            App.rootElement!.innerText = 'Failed to load data';
        } finally {
            App.loadingElement!.style.visibility = 'hidden';
        }
    }
}

export default App;
