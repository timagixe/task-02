export interface CreateElement {
    tagName: string;
    className?: string;
    attributes?: { [index: string]: string };
}

export function createElement({
    tagName,
    className,
    attributes = {},
}: CreateElement): HTMLElement {
    const element: HTMLElement = document.createElement(tagName);

    if (className) {
        const classNames: string[] = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    Object.keys(attributes).forEach((key) =>
        element.setAttribute(key, attributes[key])
    );

    return element;
}
