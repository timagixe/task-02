import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { Fighter } from '../helpers/mockData';

export function createFightersSelector() {
    let selectedFighters: Fighter[] = [];

    return async (event: MouseEvent, fighterId: string) => {
        const fighter: Fighter[] = await getFighterInfo(fighterId);
        const [playerOne, playerTwo]: Fighter[] = selectedFighters;
        const firstFighter: Fighter = playerOne ?? fighter;
        const secondFighter: Fighter = Boolean(playerOne)
            ? playerTwo ?? fighter
            : playerTwo;
        selectedFighters = [firstFighter, secondFighter];

        renderSelectedFighters(selectedFighters);
    };
}

const fighterDetailsMap: Map<string, Fighter[]> = new Map();

export async function getFighterInfo(fighterId: string): Promise<Fighter[]> {
    // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
    if (!fighterDetailsMap.has(fighterId)) {
        await fighterService.getFighterDetails(fighterId).then((response) => {
            fighterDetailsMap.set(fighterId, response);
        });
    }

    return fighterDetailsMap.get(fighterId)!;
}

function renderSelectedFighters(selectedFighters: Fighter[]): void {
    const fightersPreview: Element = document.querySelector(
        '.preview-container___root'
    )!;
    const [playerOne, playerTwo]: Fighter[] = selectedFighters;
    const firstPreview: HTMLElement = createFighterPreview(playerOne, 'left');
    const secondPreview: HTMLElement = createFighterPreview(playerTwo, 'right');
    const versusBlock: HTMLElement = createVersusBlock(selectedFighters);

    fightersPreview.innerHTML = '';
    fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: Fighter[]): HTMLElement {
    const canStartFight: boolean =
        selectedFighters.filter(Boolean).length === 2;

    const onClick: () => void = () => {
        startFight(selectedFighters);
    };

    const container: HTMLElement = createElement({
        tagName: 'div',
        className: 'preview-container___versus-block',
    });

    const image: HTMLElement = createElement({
        tagName: 'img',
        className: 'preview-container___versus-img',
        attributes: { src: versusImg },
    });

    const disabledBtn: string = canStartFight ? '' : 'disabled';
    const fightBtn: HTMLElement = createElement({
        tagName: 'button',
        className: `preview-container___fight-btn ${disabledBtn}`,
    });

    fightBtn.addEventListener('click', onClick, false);
    fightBtn.innerText = 'Fight';
    container.append(image, fightBtn);

    return container;
}

function startFight(selectedFighters: Fighter[]): void {
    renderArena(selectedFighters);
}
