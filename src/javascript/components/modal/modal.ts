import { createElement } from '../../helpers/domHelper';

interface ShowModal {
    title: string;
    bodyElement: HTMLElement;
    onClose?: () => void;
}

export function showModal({
    title,
    bodyElement,
    onClose = () => {},
}: ShowModal) {
    const root: HTMLElement = getModalContainer();
    const modal: HTMLElement = createModal({ title, bodyElement, onClose });

    root.append(modal);
}

function getModalContainer(): HTMLElement {
    return document.getElementById('root') as HTMLElement;
}

function createModal({ title, bodyElement, onClose }: ShowModal): HTMLElement {
    const layer: HTMLElement = createElement({
        tagName: 'div',
        className: 'modal-layer',
    });
    const modalContainer: HTMLElement = createElement({
        tagName: 'div',
        className: 'modal-root',
    });
    const header: HTMLElement = createHeader(title, onClose!);

    modalContainer.append(header, bodyElement);
    layer.append(modalContainer);

    return layer;
}

function createHeader(title: string, onClose: () => void): HTMLElement {
    const headerElement: HTMLElement = createElement({
        tagName: 'div',
        className: 'modal-header',
    });
    const titleElement: HTMLElement = createElement({
        tagName: 'span',
    });
    const closeButton: HTMLElement = createElement({
        tagName: 'div',
        className: 'close-btn',
    });

    titleElement.innerText = title;
    closeButton.innerText = '×';

    const close: () => void = () => {
        hideModal();
        onClose();
    };

    closeButton.addEventListener('click', close);
    headerElement.append(titleElement, closeButton);

    return headerElement;
}

function hideModal() {
    const modal: Element = document.getElementsByClassName('modal-layer')[0];
    modal?.remove();
}
