import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import type { Fighter } from '../../helpers/mockData';

export function showWinnerModal(fighter: Fighter): void {
    showModal({
        title: `${fighter.name.toUpperCase()} WON THE FIGHT`,
        bodyElement: createImage(fighter),
    });
}

function createImage(fighter: Fighter): HTMLElement {
    const { source, name }: { source: string; name: string } = fighter;
    const attributes: {
        src: string;
        title: string;
        alt: string;
    } = {
        src: source,
        title: name,
        alt: name,
    };
    const imgElement: HTMLElement = createElement({
        tagName: 'img',
        className: 'fighter___fighter-image',
        attributes,
    });

    return imgElement;
}
