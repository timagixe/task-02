import { createElement } from '../helpers/domHelper';
import { Fighter } from '../helpers/mockData';

export function createFighterPreview(
    fighter: Fighter,
    position: string
): HTMLElement {
    const positionClassName: string =
        position === 'right'
            ? 'fighter-preview___right'
            : 'fighter-preview___left';

    const fighterElement: HTMLElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });

    // todo: show fighter info (image, name, health, etc.)
    if (fighter) {
        fighterElement.appendChild(createFighterImage(fighter));
        fighterElement.appendChild(createInfoBox(fighter));
    }
    return fighterElement;
}

export function createFighterImage(fighter: Fighter): HTMLElement {
    const { source, name }: Fighter = fighter;

    const attributes: {
        src: string;
        title: string;
        alt: string;
    } = {
        src: source,
        title: name,
        alt: name,
    };

    const imgElement: HTMLElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });

    return imgElement;
}

function createInfoBox(fighter: Fighter): HTMLElement {
    const { name, health, attack, defense }: Fighter = fighter;

    const infoBox: HTMLElement = createElement({
        tagName: 'span',
        className: 'fighter-preview__infobox',
    });

    infoBox.innerText = `Name: ${name}
  Health: ${health}
  Attack: ${attack}
  Armor: ${defense}`;

    return infoBox;
}
